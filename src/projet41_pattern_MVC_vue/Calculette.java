package projet41_pattern_MVC_vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import projet41_pattern_MVC_controller.AbstractController;
import projet41_pattern_MVC_obs.Observateur;

public class Calculette extends JFrame implements Observateur {

	private JPanel container = new JPanel();
	private JLabel ecran;
	private boolean update = false;
	private boolean clicOperateur = false;
	private double valeur;
	private String operateur = "";
	private String[] listeCarac= {"7","8","9","4","5","6","1","2","3","0",".","=","C","+","-","*","/"};
	private JButton[] listeBouton = new JButton[listeCarac.length];
	private Dimension dimBouton = new Dimension (145,128);
	private Dimension dimOperateur = new Dimension(114,101 );
	private AbstractController controler;
	
	public Calculette(AbstractController pControler) {
		this.setTitle("Calculatrice");
		this.setSize(600,650);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		initComposant0();
		
		this.controler = pControler;
		this.setContentPane(container);
		this.setVisible(true);
	}
	public void initComposant0() {
		Font police = new Font("Arial", Font.BOLD, 40);
		ecran = new JLabel("0");
		ecran.setFont(police);
		ecran.setForeground(Color.BLACK);
		ecran.setHorizontalAlignment(JLabel.RIGHT);
		ecran.setPreferredSize(new Dimension(550, 60));

		JPanel panelAffichage = new JPanel();
		panelAffichage.setPreferredSize(new Dimension(580, 70));
		panelAffichage.add(ecran);
		panelAffichage.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JPanel panelOperateur = new JPanel();
		panelOperateur.setPreferredSize(new Dimension(130,515));
		JPanel panelChiffre = new JPanel();
		panelChiffre.setPreferredSize(new Dimension(470, 515));
		
		OperateurListener opeListener = new OperateurListener();
		
		for (int i=0; i<listeCarac.length; i++) {
			listeBouton[i] = new JButton(listeCarac[i]);
			listeBouton[i].setFont(police);
			listeBouton[i].setPreferredSize(dimBouton);
			switch (i) {
			case 10 : 
				listeBouton[i].addActionListener(opeListener);
				panelChiffre.add(listeBouton[i]);
				break;
			case 11 : 
				listeBouton[i].addActionListener(opeListener);
				panelChiffre.add(listeBouton[i]);
				break;
			case 12 : 
				listeBouton[i].setForeground(Color.RED);
				listeBouton[i].setPreferredSize(dimOperateur);
				listeBouton[i].addActionListener(new ResetListener());
				panelOperateur.add(listeBouton[i]);
				break;
			case 13 : 

			case 14 :
			
			case 15 : 
			
			case 16 : 
				listeBouton[i].addActionListener(opeListener);
				listeBouton[i].setPreferredSize(dimOperateur);
				panelOperateur.add(listeBouton[i]);
				break;
			default : 
				listeBouton[i].addActionListener(new ChiffreListener());
				listeBouton[i].setPreferredSize(dimBouton);
				panelChiffre.add(listeBouton[i]);
				break;
			}
			container.setBackground(Color.WHITE);
			container.setLayout(new BorderLayout());
			container.add(panelAffichage, BorderLayout.NORTH);
			container.add(panelChiffre, BorderLayout.CENTER);
			container.add(panelOperateur, BorderLayout.EAST);
		}
	}
	
	class ChiffreListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String str = ((JButton)e.getSource()).getText();
			if (!ecran.getText().equals("0"))
				str += ecran.getText();
			controler.setNombre(((JButton)e.getSource()).getText());
		}
		
	}
	class OperateurListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			controler.setOperateur(((JButton)e.getSource()).getText());
		}
	}
	class ResetListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			controler.reset();
		}
	}
	
	public void update(String str) {
		ecran.setText(str);
	}
}
