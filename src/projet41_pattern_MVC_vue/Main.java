package projet41_pattern_MVC_vue;

import projet41_pattern_MVC_controller.*;
import projet41_pattern_MVC_model.*;
import projet41_pattern_MVC_vue.Calculette;

public class Main {

	public static void main(String[] args) {
		AbstractModel calc = new Calculator();
		AbstractController controler = new CalculetteController(calc);
		Calculette c = new Calculette(controler);
		calc.addObservateur(c);

	}

}
