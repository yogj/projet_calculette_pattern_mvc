package projet41_pattern_MVC_obs;

public interface Observateur {
	public void update (String str);
}
