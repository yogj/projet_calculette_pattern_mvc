package projet41_pattern_MVC_obs;

public interface Observable {
	public void addObservateur(Observateur o);
	public void removeObservateur();
	public void notifyObservateur(String str);
	

}
