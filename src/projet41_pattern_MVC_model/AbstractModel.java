package projet41_pattern_MVC_model;

import java.util.ArrayList;

import projet41_pattern_MVC_obs.Observable;
import projet41_pattern_MVC_obs.Observateur;

public abstract class AbstractModel implements Observable{
	protected double resultat = 0;
	protected String operateur = "", operande = "";
	private ArrayList<Observateur> listObs = new ArrayList<Observateur>();
	
	public abstract void reset();
	
	public abstract void calcul();
	
	public abstract void getResultat();
	
	public abstract void setOperateur(String operateur);
	
	public abstract void setNombre(String operande);
	
	public void addObservateur(Observateur o ) {
		this.listObs.add(o);
	}
	public void removeObservateur() {
		this.listObs = new ArrayList<Observateur>();
	}
	public void notifyObservateur(String str) {
		if (str.matches("^0[0-9]+"))
			str = str.substring(1, str.length());
		for (Observateur o : listObs)
			o.update(str);
	}
	
	
}
