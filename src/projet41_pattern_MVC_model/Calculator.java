package projet41_pattern_MVC_model;

public class Calculator extends AbstractModel {

	@Override
	public void reset() {
		this.resultat = 0;
		this.operande = "";
		this.operateur = "";
		notifyObservateur(String.valueOf(this.resultat));
		
	}

	public void calcul() {
		if (this.operateur.equals(""))
			this.resultat = Double.parseDouble(this.operande);
		else {
			if (! this.operande.equals("")) {
				if(this.operateur.equals("+"))
					this.resultat += Double.parseDouble(this.operande);
				else if (this.operateur.equals("-"))
					this.resultat -= Double.parseDouble(this.operande);
				else if (this.operateur.equals("*"))
					this.resultat *= Double.parseDouble(this.operande);
				else if (this.operateur.equals("/")) {
					try {
						this.resultat /= Double.parseDouble(this.operande);
					}catch (ArithmeticException e) {
						this.resultat = 0;
					}
				}
			}
		}
		this.operande = "";
		notifyObservateur(String.valueOf(this.resultat));
		
	}

	public void getResultat() {
		calcul();
		
	}

	public void setOperateur(String pOperateur) {
		calcul();
		this.operateur = pOperateur;
		if(! pOperateur.equals("="))
			this.operande = "";
		
	}

	public void setNombre(String pOperande) {
		this.operande += pOperande;
		notifyObservateur(this.operande);
		
	}
	

}
