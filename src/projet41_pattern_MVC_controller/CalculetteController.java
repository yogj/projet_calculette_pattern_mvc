package projet41_pattern_MVC_controller;

import projet41_pattern_MVC_model.AbstractModel;

public class CalculetteController extends AbstractController {
	
	public CalculetteController(AbstractModel cal) {
		super(cal);
	}
	
	public void control() {
		//On notifie le mod�le d'une action si le contr�le est bon
		//--------------------------------------------------------
		
		if(this.listOperateur.contains(this.operateur)) {
			if(this.operateur.equals("="))
				this.calc.getResultat();
			else
				this.calc.setOperateur(this.operateur);
		}
		if(this.nbre.matches("^0[0-9]+$"))
			this.calc.setNombre(this.nbre);
		
		this.operateur = "";
		this.nbre = "";
			
	}

}
