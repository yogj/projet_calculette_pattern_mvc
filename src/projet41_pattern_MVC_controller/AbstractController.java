package projet41_pattern_MVC_controller;

import java.util.ArrayList;

import projet41_pattern_MVC_model.AbstractModel;

public abstract class AbstractController {
	protected AbstractModel calc;
	protected String operateur="", nbre = "";
	protected ArrayList<String> listOperateur = new ArrayList<String>();
	
	public AbstractController(AbstractModel cal) {
		this.calc = cal;
		this.listOperateur.add("+");
		this.listOperateur.add("-");
		this.listOperateur.add("*");
		this.listOperateur.add("/");
		this.listOperateur.add("=");
	}
	
	public void setOperateur(String ope) {
		this.operateur = ope;
		control();
	}
	public void setNombre(String nb) {
		this.nbre = nb;
		control();
	}
	public void reset() {
		this.calc.reset();
	}
	
	public abstract void control();
		
}
